document.addEventListener('DOMContentLoaded', () => {
  const chatHistroy = document.getElementById('chat-history')
  const message = document.getElementById('message')
  const roomTitle = document.getElementById('room-title')
  const roomId = localStorage.getItem('sy-messenger-roomId')
  const ws = new WebSocket('ws://localhost:8000')
  const roomAction = localStorage.getItem('sy-messenger-action')
  let userId;

  localStorage.setItem('sy-messenger-action', 'join')
  roomTitle.innerText = roomId
  document.title = `sy 메신저 | ${roomId}`

  ws.onopen = e => {
    switch (roomAction) {
      case 'create':
        ws.send(JSON.stringify({
          action: 'create',
          data: {
            roomId: roomId
          }
        }))

      case 'join':
        ws.send(JSON.stringify({
          action: 'join',
          data: {
            roomId: roomId
          }
        }))

        break
    }

    message.addEventListener('keypress', e => {
      if (e.ctrlKey && e.key === 'Enter') {
        ws.send(JSON.stringify({
          action: 'chat',
          data: {
            message: message.value,
            roomId: roomId
          }
        }))
        message.value = ''
      }
    })
  }

  ws.onmessage = e => {
    const data = JSON.parse(e.data)
    const shouldScrollToBottom = chatHistroy.scrollHeight - chatHistroy.scrollTop === chatHistroy.clientHeight

    switch (data.action) {
      case 'initialData':
        userId = data.data.userId
        addChatBox('시스템', `${userId}님 환영합니다`)
        break

      case 'join':
        addChatBox('시스템', `${data.data.roommateId}님이 입장하셨습니다`)
        break


      case 'quit':
        addChatBox('시스템', `${data.data.roommateId}님이 퇴장하셨습니다`)
        break

      case 'chat':
        addChatBox(data.data.from, data.data.message)
        break
    }

    shouldScrollToBottom && chatHistroy.scrollTo(0, chatHistroy.scrollHeight)
  }

  function addChatBox(from, message) {
    const chatBox = document.createElement('div')
    chatBox.classList.add('chat-box')

    const chatFrom = document.createElement('div')
    chatFrom.classList.add('chat-from')
    chatFrom.innerText = from

    const chatMessage = document.createElement('div')
    chatMessage.classList.add('chat-message')
    chatMessage.innerText = message

    chatBox.appendChild(chatFrom)
    chatBox.appendChild(chatMessage)

    chatHistroy.appendChild(chatBox)
  }
})

