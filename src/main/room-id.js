document.addEventListener('DOMContentLoaded', () => {
  const roomId = document.getElementById('room-id')
  const join = document.getElementById('join')
  const create = document.getElementById('create')

  join.addEventListener('click', e => {
    e.preventDefault()

    roomExists(roomId.value)
      .then(result => {
        if (!result) {
          alert('방이 존재하지 않습니다')
        } else {
          localStorage.setItem('sy-messenger-roomId', roomId.value)
          localStorage.setItem('sy-messenger-action', 'join')
          location.href = 'chat.html'
        }
      }).catch(() => {
        alert('오류가 발생했습니다\n잠시 후에 다시 시도해주세요')
      })
  })

  create.addEventListener('click', e => {
    e.preventDefault()

    roomExists(roomId.value)
      .then(result => {
        if (result) {
          alert('방이 이미 존재합니다')
        } else {
          localStorage.setItem('sy-messenger-roomId', roomId.value)
          localStorage.setItem('sy-messenger-action', 'create')
          location.href = 'chat.html'
        }
      }).catch(() => {
        alert('오류가 발생했습니다\n잠시 후에 다시 시도해주세요')
      })
  })
})

function roomExists(roomId) {
  return new Promise((resolve, reject) => {
    let formData = new FormData()
    formData.append('roomId', roomId)

    fetch(
      'http://localhost:8000/room-exists',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'text/plain',
        },
        mode: 'cors',
        body: JSON.stringify({
          roomId: roomId
        })
      }
    ).then(res => {
      return res.json()
    }).then(data => {
      resolve(data.exists)
    }).catch(() => {
      reject()
    })
  })
}

